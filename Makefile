GCC=gcc
CFLAGS = -Wall -g
TARGET = uart_test

all:uart_read_write.c
	$(GCC) $(CFLAGS) -o $(TARGET) uart_read_write.c

clean:
	-rm -f $(TARGET)
