# UART read write Linux

Sample UART  Programming Guide for POSIX Operating Systems, Mainly Linux.This very simple code for UART Porgramming.Test on Ubuntu 18.04.

## Usage
```shell
git clone https://gitlab.com/anoopknr/uart-read-write-linux.git # or download source code
cd uart-read-write-linux
make
```
## Learn More
 - Serial Programming [Link](https://www.cmrr.umn.edu/~strupp/serial.html)
