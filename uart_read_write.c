#include <errno.h>   /* Error number definitions */
#include <fcntl.h>   /* File control definitions */
#include <stdio.h>   /* Standard input/output definitions */
#include <stdlib.h>  /*Standard library definitions */
#include <string.h>  /* String function definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <unistd.h>  /* UNIX standard function definitions */

#define BAUD_115200 1
#define BAUD_57600 2
#define BAUD_38400 3
#define BAUD_19200 4
#define BAUD_9600 5

#define P_NONE 0
#define P_ODD 1
#define P_EVEN 2

#define STOP_BITS_1 0
#define STOP_BITS_2 1

#define DATA_BITS_8 0
#define DATA_BITS_7 1
#define DATA_BITS_6 2
#define DATA_BITS_5 3

#define DELAY 2

/*
 * 'open_port()' - Open serial port 1.
 * Returns the file descriptor on success or -1 on error.
 */
int open_port(char *port_name) {
  int fd; /* File descriptor for the port */
  char err_str[100] = "Unable to open ";

  fd = open(port_name, O_RDWR | O_NOCTTY | O_NDELAY);
  if (fd == -1) {
    // Could not open the port.
    strcat(err_str, port_name);
    perror(err_str);
    exit(0);
  } else
    fcntl(fd, F_SETFL, 0);
  return (fd);
}

/*
 * 'set_options()' - Configuring the Serial Port.
 * Returns the file descriptor on success or 0 on error.
 */
int set_options(int fd, int baud, int parity, int data_size, int stop_bits) {
  char uart_mode[100] = "";

  struct termios options;
  // Get the current options for the port...
  tcgetattr(fd, &options);
  // Set the baud rates
  switch (baud) {
  case BAUD_115200:
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    strcat(uart_mode, "115200 ");
    break;
  case BAUD_57600:
    cfsetispeed(&options, B57600);
    cfsetospeed(&options, B57600);
    strcat(uart_mode, "38400 ");
    break;
  case BAUD_38400:
    cfsetispeed(&options, B38400);
    cfsetospeed(&options, B38400);
    strcat(uart_mode, "38400 ");
    break;
  case BAUD_19200:
    cfsetispeed(&options, B19200);
    cfsetospeed(&options, B19200);
    strcat(uart_mode, "38400 ");
    break;
  case BAUD_9600:
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    strcat(uart_mode, "9600 ");
    break;
  }
  // setting data bits
  options.c_cflag &= ~CSIZE; /* Mask the character size bits */
  switch (data_size) {
  case DATA_BITS_8:
    options.c_cflag |= CS8; /* Select 8 data bits */
    strcat(uart_mode, "8");
    break;
  case DATA_BITS_7:
    options.c_cflag |= CS7; /* Select 7 data bits */
    strcat(uart_mode, "7");
    break;
  case DATA_BITS_6:
    options.c_cflag |= CS6; /* Select 6 data bits */
    strcat(uart_mode, "6");
    break;
  case DATA_BITS_5:
    options.c_cflag |= CS5; /* Select 5 data bits */
    strcat(uart_mode, "5");
    break;
  }
  // setting parity
  switch (parity) {
  case P_NONE:
    options.c_cflag &=
        ~PARENB; // Disables the Parity Enable bit(PARENB),So No Parity   */
    strcat(uart_mode, "N");
    break;

  case P_ODD:
    options.c_cflag |= PARENB;
    options.c_cflag |= PARODD;
    strcat(uart_mode, "O");
    break;

  case P_EVEN:
    options.c_cflag |= PARENB;
    options.c_cflag &= ~PARODD;
    strcat(uart_mode, "E");
    break;
  }
  // setting stop bits
  switch (stop_bits) {
  case STOP_BITS_1:
    options.c_cflag &=
        ~CSTOPB; // CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    strcat(uart_mode, "1");
    break;

  case STOP_BITS_2:
    options.c_cflag |= CSTOPB; // CSTOPB = 2  */
    strcat(uart_mode, "2");
    break;
  }
  options.c_cflag &= ~CRTSCTS; // No Hardware flow Control */
  options.c_cflag |=
      (CREAD | CLOCAL); // Enable receiver,Ignore Modem Control lines       */
  options.c_iflag &=
      ~(IXON | IXOFF |
        IXANY); // Disable XON/XOFF flow control both i/p and o/p */
  options.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); // Non Cannonical mode */
  options.c_oflag &= ~OPOST;                          // No Output Processing*/
  // Set the new options for the port...
  if ((tcsetattr(fd, TCSANOW, &options)) !=
      0) { /* Set the attributes to the termios structure */
    printf("\n Failed setting attributes !");
    return 0;
  }
  printf("\n+-----------+-----------------+");
  printf("\n| UART Mode : %14s  |", uart_mode);
  printf("\n+-----------+-----------------+\n");
  return 1;
}

void send_data(int fd, char *data, int d_size) {
  int bytes_send = 0;
  bytes_send = write(fd, data, d_size);
  if (bytes_send < 0)
    fputs("send_data() Failed ! \n", stderr);
}

int receive_data(int fd, char *read_buffer, int d_size) {
  int bytes_read = 0; /* Number of bytes read by the read() system call */
  bytes_read = read(fd, &read_buffer, sizeof read_buffer); /* Read the data */
  return bytes_read;
}

int main() {
  int fd = open_port("/dev/ttyUSB0");

  set_options(fd, BAUD_115200, P_NONE, DATA_BITS_8, STOP_BITS_1);
  tcflush(fd, TCIFLUSH); /* Discards old data in the rx buffer            */

  send_data(fd, "test", 4);
  sleep(DELAY);

  char read_buffer[32]; /* Buffer to store the data received */
  int bytes_read = receive_data(fd, &read_buffer, sizeof read_buffer);

  printf("\nBytes Rxed :%d\n", bytes_read); /* Print the number of bytes read */

  for (int i = 0; i < bytes_read; i++) /*printing only the received characters*/
    printf("%c", read_buffer[i]);

  return 0;
}
